

HAMRS ADI Converter
===================



Background
----------

For FieldDay 2022, I operated alone as a 1D station.  I had been poking around
for Linux software for operating Amateur Radio, but I'm Finding the software
that Great Oracle Google provides links to are lacking...

[Skookum](https://k1gq.net/SkookumLogger/) is a frickin awesome logger, but 
Mac only.  My last Mac is old and dying and gave up its discrete GPU.  It's
not a good candidate for being out in the field during Field Day.

[N1MM+](https://n1mmwp.hamdocs.com/) is, unfortunately, Windows only.  Its
rig control and whatnot is very tempermental as well.  It's been a few years
since I've personally used it, I do not know if it has switched to using
hamlib or not.

So... in my brilliance, I chose to use [HAMRS](https://hamrs.app/), which
has no hamlib/radio interfacing, no networking, etc.  It's a very very very
basic logging application that looks pretty.  It worked well enough for
Field Day this year.

HAMRS supports export to ADI.  There's no other format or export capability.

Viewing the logs in HAMRS itself is sufficient, but very very limited.  To
be fair, the logging software is somewhat new and barely a 1.0 release.  I'm
actually pretty interested to see what this software can turn into.

Today, however, getting my logs from the ADI format to something useable as
a dupe sheet for ARRL Field Day submission was less than spectacular.

Thus, I created the ConvertADI.py script.



Overview
--------

This is not a true ADI file format converter.

ConvertADI.py is very ghetto, and is very much a crap software script that
was quickly and poorly written to solve an immediate need for submission.

ConvertADI will take the HAMRS ADI export, massage the data in a local
SQLite3 database, and dump out usable details to fill into the ARRL Field
Day online submission form.

It is expected to be run using Python 3.7.4 or later



Usage
-----

### Overview

0 - Tweak ConvertADI for running on Windows
1 - Export ADI out of HAMRS
2 - Run ConvertADI


### Tweak COnvertADI for running on Windows

Line 20 in ConvertADI.py specifies the SQLITE_DB_FILE value as a linux/mac
file path of `/tmp/field_day.db`.  Update this to `C:\TEMP\field_day.db`.

Line 23 in ConvertADI.py specifies the ADI_FILENAME value as a linux/mac
file path of `/tmp/FieldDay.adi`.  Update this to `C:\TEMP\FieldDay.adi`


### Export ADI out of HAMRS

In HAMRS application, go to Logbooks.  Find the Logbook used for Field Day
and click on the little gear icon and then click on _Export .ADI_ and save
the ADI file.  Save and/or move the ADI file to `/tmp/FieldDay.adi` on
a Mac or Linux machine.  If on Windows, make sure that `C:\TEMP` exists
and then save and/or move the ADI file to `C:\TEMP\FIeldDay.adi`.

Remember that Linux is case sensitive.  Mac OS filesystems _could_ be case
sensitive.  Windows file systems are case-aware.  To make things easier on
yourself, make the filenames exactly the correct name and case.


### Run ConvertADI

Remember what I said about this script being ghetto? It's ghetto.  Nothing
is optioned and it's hard-coded in the header of the file.  Put on a crash
helmet if you have one and then run the Python script:

    python3 ConvertADI.py

... wait for the results.

Here's my FieldDay 2022 results as an example:

	2022-06-28 19:42:56,063 [INFO]: Report: 
	[('80m', 3.573, 'data', 'FT8', '20220626', '072945', 'AB5ER', '2F', 'AR'),

	[...SNIP...]

	 ('15m', 21.074, 'data', 'FT8', '20220625', '183950', 'VE7NBQ', '1D', 'BC')]
	2022-06-28 19:42:56,064 [INFO]: Contact Count Report: 
	[(12, '80m', 'data'),
	 (6, '80m', 'phone'),
	 (22, '40m', 'data'),
	 (29, '40m', 'phone'),
	 (26, '20m', 'data'),
	 (4, '20m', 'phone'),
	 (1, '15m', 'data')]
	2022-06-28 19:42:56,065 [INFO]: Total QSOs: [(100,)]



License
-------

MIT License  See [LICENSE File](./LICENSE)
