

import sqlite3
from contextlib import closing
import os
import re
from pprint import PrettyPrinter
import logging

# /* ************
#  * ************
#  *
#  *  Static Settings
#  *  (ghetto processing)
#  *
#  * ************
#  * ************/

# SQLite Database file
SQLITE_DB_FILE = "/tmp/field_day.db"

# ADI File Path
ADI_FILENAME = "/tmp/FieldDay.adi"

LOG_LEVEL = logging.INFO

# /* DO NOT EDIT BELOW THIS LINE */

logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s', level=LOG_LEVEL)

#DBCONNECTION = sqlite3.connect(os.path.realpath(os.path.abspath(SQLITE_DB_FILE)))

DB_SCHEMA = """
CREATE TABLE qsos (qso_id INTEGER PRIMARY KEY AUTOINCREMENT,
   band_khz REAL NOT NULL,
   mode_primary TEXT NOT NULL,
   mode_submode TEXT NOT NULL,
   utc_date TEXT NOT NULL,
   utc_time TEXT NOT NULL,
   callsign TEXT NOT NULL,
   band_name TEXT NOT NULL,
   call_class TEXT NOT NULL,
   call_section TEXT NOT NULL);
"""


def check_database_schema():
    if exec_tsql(".schema qsos") != DB_SCHEMA:
        try:
            create_db_schema()
        except Exception as e:
            raise e

def create_db_schema():
    logging.debug("Starting create_db_schema()")

    sql_cmd = DB_SCHEMA.replace("CREATE TABLE ", "CREATE TABLE IF NOT EXISTS ")
    logging.debug("sql_cmd: {}".format(sql_cmd))

    exec_tsql(sql_cmd)

def exec_tsql(sql_cmd: str, value_tuple: tuple=()):
    logging.debug("Starting exec_tsql()")
    return exec_tsql_v2(sql_cmd, value_tuple)

def exec_tsql_v1(sql_cmd: str, value_tuple: tuple=()):
    logging.debug("Starting exec_tsql_v1()")

    connection = sqlite3.connect(os.path.realpath(os.path.abspath(SQLITE_DB_FILE)))

    #cursor = DBCONNECTION.cursor()
    cursor = DBCONNECTION.cursor()
    try:
        if value_tuple:
            logging.debug("Found Value Tuple, Executing SQL Cmd [{}] with Tuple [{}]".format(sql_cmd, value_tuple))
            cursor.execute(sql_cmd, value_tuple)
        else:
            logging.debug("Executing SQL Cmd: {}".format(sql_cmd))
            cursor.execute(sql_cmd)
    except Exception as e:
        raise e
    finally:
        cursor.close()


def exec_tsql_v2(sql_cmd: str, value_tuple: tuple=(), database_file=SQLITE_DB_FILE):
    logging.debug("Starting exec_tsql_v2()")
    with closing(sqlite3.connect(os.path.realpath(os.path.abspath(database_file)))) as connection:
        with closing(connection.cursor()) as cursor:
            if value_tuple:
                logging.debug("Found Value Tuple, Executing SQL Cmd [{}] with Tuple [{}]".format(sql_cmd, value_tuple))
                returnables = cursor.execute(sql_cmd, value_tuple).fetchall()
            else:
                logging.debug("Executing SQL Cmd: {}".format(sql_cmd))
                returnables = cursor.execute(sql_cmd).fetchall()
            connection.commit()

    return returnables


def clear_database_qsos():
    exec_tsql("DELETE FROM qsos")


def handle_entry_line(line: str) -> dict:
    logging.debug("Starting handle_entry_line()")

    dont_care_shiz = [
            "<eor>",
            "<EOH>",
            "Generated on ",
            "<programid:",
            "<grogramversion:",
        ]
    if line == os.linesep:
        logging.debug("Don't care about blank lines")
        raise LookupError("Don't care about blank lines".format(line))

    if line.startswith("<notes:"):
        logging.debug("Don't care about '<notes:' lines")
        raise LookupError("notes turd found")

    for dont_care in dont_care_shiz:
        if line.startswith(dont_care):
            logging.debug("Don't care about: {}".format(dont_care))
            raise LookupError("Don't care about line item: {}".format(line))

    # Initializer
    adi_line = None

    # Case 1
    if not adi_line:
        adi_line = re.match(r'<(?P<field_id>\w+):(?P<field_length>\d+)>(?P<field_value>[a-zA-Z0-9_., ]*)', line)
    # Case 2
    if not adi_line:
        adi_line = re.match(r'<(?P<field_id>\w+):(?P<field_length>\d+)>(?P<field_value>(.*))', line)
    
    if adi_line:
        if len(str(adi_line.group('field_value'))) == int(adi_line.group('field_length')):
            return { adi_line.group('field_id'): adi_line.group('field_value') }

    raise ValueError("Do not understand how to parse {}".format(line))


def insert_qso(qso_dict: dict):
    pp = PrettyPrinter()
    logging.debug("Handling QSO: {}".format(pp.pformat(qso_dict)))

    sql_command = "INSERT INTO qsos (band_khz, band_name, mode_primary, mode_submode, utc_date, utc_time, callsign, call_class, call_section) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)" 
    sql_values = (
        qso_dict.get('freq', 0.0),
        qso_dict.get('band', "None"),
        qso_dict.get('mode_primary', "None"),
        qso_dict.get('mode_submode', "None"),
        qso_dict.get('qso_date', "None"),
        qso_dict.get('time_on', "None"),
        qso_dict.get('call', "None"),
        qso_dict.get('class', "None"),
        qso_dict.get('arrl_sect', "None")
    )

    logging.debug("Executing SQL Command: {}".format(sql_command))
    logging.debug("Providing SQL Values: {}".format(pp.pprint(sql_values)))

    # Call sql_exec()
    exec_tsql(sql_command, sql_values)
    

def handle_entry_section(section_blob):
    logging.debug("Starting handle_entry_section()")
    qso = {}
    for item in section_blob:
        try:
            entry_item = handle_entry_line(item)
            if "mode" in entry_item.keys():
                this_mode_type = str(entry_item.get("mode", ""))

                # assume data, because we know phone and cw types
                mode_class = "data"
                
                phone_mode_types = ["lsb", "usb", "am", "fm", "ssb"]
                if this_mode_type.lower() in phone_mode_types:
                    mode_class = "phone"
                if "cw" == this_mode_type.lower():
                    mode_class = "cw"

                qso['mode_primary'] = mode_class
                qso['mode_submode'] = this_mode_type

            # Shove the direct ADIF field name and field value into the dict as well
            entry_dict = handle_entry_line(item)
            for key in entry_dict.keys():
                logging.debug("Adding in K,V to qso: {}, {}".format(key, entry_dict[key]))
                qso[key] = entry_dict[key]

        except LookupError as e:
            logging.debug("Ignoring Line Item: {}".format(item))
            pass
        except Exception as e:
            raise e

    insert_qso(qso)


def handle_entry_file(filename):
    logging.debug("Starting handle_entry_file()")
    filecontents = []
    with open(os.path.abspath(os.path.realpath(filename)), 'r') as fh:
        filecontents = fh.readlines()

    logging.debug("filecontents: {}".format(filecontents))

    section = []
    for line in filecontents:
        if line == os.linesep:
            # New Entry Section
            section = []

        if line.startswith('<EOH>'):
            # End of header section, handle header
            pass

        if line.startswith('<eor>'):
            # end of record, pass to section handler
            handle_entry_section(section)

        section.append(line)


def pretty_up_the_shiz():
    pp = PrettyPrinter()

    sql_cmd = """
                 SELECT
                   band_name,
                   band_khz,
                   mode_primary,
                   mode_submode,
                   utc_date,
                   utc_time,
                   callsign,
                   call_class,
                   call_section
                 FROM qsos
                 -- GROUP BY band_name, mode_primary
                 ORDER BY band_khz, mode_primary, callsign"""

    logging.info("Report: {}".format(os.linesep + pp.pformat(exec_tsql(sql_cmd))))


def display_worked_table():
    pp = PrettyPrinter()

    sql_cmd = """
                 SELECT 
                   count(callsign),
                   band_name,
                   mode_primary 
                FROM qsos 
                GROUP BY band_name,mode_primary 
                ORDER BY band_khz,mode_primary"""
    logging.info("Contact Count Report: {}".format(os.linesep + pp.pformat(exec_tsql(sql_cmd))))


def display_total_qsos():
    logging.info("Total QSOs: {}".format(str(exec_tsql("SELECT count(*) FROM qsos"))))


if __name__ == '__main__':
    #check_database_schema()
    create_db_schema()
    clear_database_qsos()
    handle_entry_file(ADI_FILENAME)
    pretty_up_the_shiz()
    display_worked_table()
    display_total_qsos()
